// Copyright (c) 2010-2019 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D® SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT

// TextureandEffectRender

///----------------------------------------------------------------------------
/// README: 
/// SETUP Prepar3D Configuration
/// This example required additional setup in Prepar3D configuration
/// [dll.xml]
/// [panel.CFG]
/// [Camera.CFG]
///----------------------------------------------------------------------------

#include "PdkServices.h"
#include "D3D12SampleInlineRenderer.h"
#include <fstream>

#define     RTT_WIDTH       200
#define     RTT_HEIGHT      200

using namespace P3D;

extern "C" __declspec(dllexport) void __stdcall DLLStart(__in __notnull IPdk* pPdk)
{
    {
        std::ofstream o("f:\\prepar3d_vioso.log");
        o << "VIOSO_Plugin DllStart" << std::endl;
    }
    PdkServices::Init(pPdk);

    CComPtr<IRenderingPluginSystemV500> spPluginSystem = nullptr;    // The Rendering Plugin System 
    CComPtr<IRenderingPluginV500> spTexturePlugin = nullptr;         // The Plugin to create a new texture
    CComPtr<IRenderingPluginV500> spEffectPlugin = nullptr;          // The Plugin to create a new effect

    // Request the Rendering System Plugin from the PDK Services.
    spPluginSystem = PdkServices::GetRenderingPluginSystem();

    // The plugin service is then used to Create a new Texture for Prepar3D
    // This texture will simply contain a Cursor on a blue background,
    // however anything could be rendered into it using DirectX11.

    // Create and register the sample rendering plugin as a texture.
    spTexturePlugin.Attach(new D3D12SampleInlineRenderer(false));
    spPluginSystem->CreateTexture(TEXT("VIOSOWarpBlend"), RTT_WIDTH, RTT_HEIGHT, spTexturePlugin);

    // Create a new Effect in Prepar3D, this draw a cursor on top of a view
    // Again this is a simple example but anything can be rendered using DirectX11.
    // The current view output can also be requested allowing a custom effect to act as
    // an advanced post process which modifies the view output rather than simply
    // adding content on top of it

    // Create and register the sample rendering plugin as an effect.
    spEffectPlugin.Attach(new D3D12SampleInlineRenderer(true));
    spPluginSystem->CreateEffect(TEXT("VIOSOWarpBlend"), spEffectPlugin);

    {
        std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
        o << "VIOSO_Plugin init finished" << std::endl;
    }

    return;
}

extern "C" __declspec(dllexport) void __stdcall DLLStop(void)
{
    {
        std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app );
        o << "VIOSO_Plugin DllStop" << std::endl;
    }
    CComPtr<IRenderingPluginSystemV500> spPluginSystem = nullptr;   // The Rendering Plugin System 

    spPluginSystem = PdkServices::GetRenderingPluginSystem();

    // Unregister our texture and effect.
    spPluginSystem->RemoveTexture(TEXT("VIOSOWarpBlend"));
    spPluginSystem->RemoveEffect(TEXT("VIOSOWarpBlend"));

    PdkServices::Shutdown();
}
