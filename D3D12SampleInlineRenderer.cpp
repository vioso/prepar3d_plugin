// Copyright (c) 2010-2019 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D® SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT

// D3D12SampleInlineRenderer.cpp
// Description: This Example will render a cursor into Prepar3D as a new Texture.

#include <d3dcompiler.h>

#include "Helpers/initpdk.h"
#include "D3D12SampleInlineRenderer.h"
#include "IWindowPluginSystem.h"
#include <fstream>
#include <filesystem>
#include <locale>
#include <codecvt>
using namespace P3D;


#ifdef USE_VIOSO_API
#define VIOSOWARPBLEND_DYNAMIC_IMPLEMENT
#include "VIOSOWarpBlend.h"
LPCTSTR s_configFile = _T( "VIOSOWarpBlend.ini" );
#endif //def USE_VIOSO_API


D3D12SampleInlineRenderer::D3D12SampleInlineRenderer(bool bIsEffect)
    : m_bIsEffect(bIsEffect)
#ifdef USE_VIOSO_API
    , m_pRenderTargetRef(nullptr)
#endif
{
}

D3D12SampleInlineRenderer::~D3D12SampleInlineRenderer()
{
    if( VWB_Destroy )
    {
        for( auto& c : m_warpers )
        {
            if( c.second )
            {
                VWB_Destroy( c.second );
            }
        }
    }
}

//--------------------------------------------------------------------------------------
// Render callback
//--------------------------------------------------------------------------------------
void D3D12SampleInlineRenderer::Render(IRenderDataV500* pRenderData)
{
    IRenderDataV500* pRenderDataV500 = static_cast<IRenderDataV500*>(pRenderData);



    if (!pRenderDataV500)
    {
        return;
    }
    CComPtr spWindow = pRenderData->GetWindow();
    if( nullptr != spWindow )
    {
        CComPtr spRenderTarget = pRenderDataV500->GetOutputColor();
        CComPtr spRenderSource = pRenderDataV500->GetInputColor();

        if (spRenderTarget == NULL || spRenderSource == NULL)
        {
            return;
        }
        if (m_spRtvHeap == NULL)
        {
            D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
            rtvHeapDesc.NumDescriptors = 4;
            rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
            rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
            pRenderData->GetDevice()->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_spRtvHeap));
        }
        CComPtr spRT = spRenderTarget->GetD3D12Resource();
        if (m_pRenderTargetRef != spRT)
        {
            pRenderData->GetDevice()->CreateRenderTargetView(spRT, nullptr, m_spRtvHeap->GetCPUDescriptorHandleForHeapStart());
            m_pRenderTargetRef = spRT;
        }

        VWB_D3D12_RENDERINPUT ri = { spRenderSource->GetD3D12Resource(), m_pRenderTargetRef, m_spRtvHeap->GetCPUDescriptorHandleForHeapStart().ptr };
        auto f = m_warpers.find( spWindow );
        if( m_warpers.end() != f && nullptr != f->second )
        {
            VWB_render( f->second, &ri, 0 );
        }
    }
}

//--------------------------------------------------------------------------------------
// PreRender callback
//--------------------------------------------------------------------------------------
void D3D12SampleInlineRenderer::PreRender(P3D::IRenderDataV500* pRenderData)
{
    if (!pRenderData)
    {
        return;
    }

    // Disable rendering until our device is initialized.
    if ( m_spCommandQueue == NULL)
    {
        mRenderFlags.RenderingIsEnabled = false;
        pRenderData->SetRenderFlags(mRenderFlags);
    }

    CComPtr<ID3D12CommandQueue> spCQ = pRenderData->GetCommandQueue( D3D12_COMMAND_LIST_TYPE_DIRECT );
    if ( spCQ == NULL)
    {
        {
            std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
            o << "VIOSO_Plugin no CommandQueue" << std::endl;
        }
        return;
    }
    if (pRenderData->GetRenderPass() != RenderPassPrimaryThreadUpdate && pRenderData->GetWindow() )
    {
#ifdef USE_VIOSO_API

        {
            std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
            o << "VIOSO_Plugin PreRender: intst " << this << " renderData " << pRenderData << " Wnd " << pRenderData->GetWindow() << " Cam " << pRenderData->GetCamera() << std::endl;
        }

        {
            std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
            o << "VIOSO_Plugin before dynamic init" << std::endl;
        }

        #define VIOSOWARPBLEND_FILE name
        TCHAR name[MAX_PATH] = { 0 };
        GetModuleFileName(GetModuleHandle(_T("VIOSO_Plugin")), name, MAX_PATH);
        TCHAR* p = _tcsrchr(name, '\\');
        _tcscpy_s(p, MAX_PATH - (p - name), _T("\\VIOSOWarpBlend64.dll"));
        {
#ifdef UNICODE

            char s[MAX_PATH];
            wcstombs_s( nullptr, s, name, _TRUNCATE );
#else
            char const* s = name;
#endif //def UNICODE
            std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
            o << "VIOSO_Plugin try loading \"" << s << "\"" << std::endl;
        }

        if (nullptr == VWB_Create)
        {
#define VIOSOWARPBLEND_DYNAMIC_INITIALIZE
#include "VIOSOWarpBlend.h"
        }

        if (
            nullptr == VWB_Create ||
            nullptr == VWB_Init)
        {
            {
                std::ofstream o("f:\\prepar3d_vioso.log", std::ios::app);
                o << "VIOSO_Plugin dynamic load failed" << std::endl;
            }
            return;
        }
        #endif //def USE_VIOSO_API

        m_spCommandQueue = spCQ;
    }
    else if( VWB_Create )
    {
        CComPtr spWindow = pRenderData->GetWindow();
        if( nullptr != spWindow )
        {
            VWB_Warper*& pW = m_warpers.emplace( spWindow, nullptr ).first->second;
            if( nullptr == pW )
            {
                LPCTSTR nameWnd = spWindow->GetWindowName();
                if(
                    VWB_ERROR_NONE != VWB_Create( m_spCommandQueue, s_configFile, nameWnd, &pW, 0, NULL ) ||
                    VWB_ERROR_NONE != VWB_Init( pW )
                    )
                {
                    {
                        std::ofstream o( "f:\\prepar3d_vioso.log", std::ios::app );
                        o << "VIOSO_Plugin VWB_Create failed" << std::endl;
                    }
                    return;
                }
            }
            // ToDO: get frustum
            CComPtr spCam = pRenderData->GetCamera();

            float symClip[4];
            spCam->GetFov( symClip[0], symClip[1] );
            symClip[2] = spCam->GetNearClip();
            symClip[3] = spCam->GetFarClip();
            float pos[3];
            spCam->GetCameraOffset( pos[0], pos[1], pos[2] );
            float dir[3]; // direction euler angles; around x = pitch, z = roll/bank, y = yaw/heading
            spCam->GetBiasPBH( dir[0], dir[2], dir[1] );

            float eye[3] = { 0 };
            float rot[3] = { 0 };
            if (VWB_ERROR_NONE == VWB_getPosDirFov(pW, eye, rot, pos, dir, symClip))
            {
                spCam->SetFov( symClip[0] * 180.0f / 3.14159265358979323846f, symClip[1] * 180.0f / 3.14159265358979323846f );
                spCam->SetNearClip(symClip[2]);
                spCam->SetFarClip(symClip[3]);
                spCam->SetRelative6DOF(pos[0], pos[1], pos[2], dir[0], dir[2], dir[1]);
            }
        }
    }

    // Only want Render() called during the default render pass
    mRenderFlags.RenderingIsEnabled = (pRenderData->GetRenderPass() == RenderPassDefault);
    
    // Will not read the color render target.
    mRenderFlags.WillReadColor = true;

    pRenderData->SetRenderFlags(mRenderFlags);
}
