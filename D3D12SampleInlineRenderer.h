// Copyright (c) 2010-2019 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D® SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT

// D3D12SampleInlineRenderer
// Description: This Example will render a cursor into Prepar3D as a new Texture or as an Effect.  

#define USE_VIOSO_API

#ifndef D3D12SAMPLEINLINERENDERER_H
#define D3D12SAMPLEINLINERENDERER_H

#pragma once

#include "IUnknownHelper.h"
#include "RenderingPlugin.h"

#pragma warning ( push )
#pragma warning (disable : 4005)
#include <d3d12.h>
#include "d3dx12.h"
#pragma warning( pop )

#pragma warning( push )
#pragma warning( disable : 4838)
#include <DirectXMath.h>
#pragma warning( pop )

#ifdef USE_VIOSO_API
#define VIOSOWARPBLEND_DYNAMIC_DEFINE
#include "VIOSOWarpBlend.h"
#include <memory>
#include <map>
#endif

// Simple d3d12 renderer which implements the custom render callback interface
// so that it can be registered with Prepar3D for rendering custom textures
// and effects
class D3D12SampleInlineRenderer : public P3D::RenderingPlugin
{
public:

    struct Vertex
    {
        DirectX::XMFLOAT3 position;
    };

    D3D12SampleInlineRenderer(bool bIsEffect);
    ~D3D12SampleInlineRenderer();

    /* IRenderingPluginV400 */
    virtual void Render(P3D::IRenderDataV500* pRenderData) override;
    virtual void PreRender(P3D::IRenderDataV500* pRenderData) override;

private:

    CComPtr<ID3D12CommandQueue>         m_spCommandQueue;
    CComPtr<ID3D12DescriptorHeap>       m_spRtvHeap;
    bool                                m_bIsEffect;

    #ifdef USE_VIOSO_API
      ID3D12Resource*                     m_pRenderTargetRef;
      std::map< P3D::IWindowV400 const*, VWB_Warper* > m_warpers;
    #endif
};

#endif
