// Copyright (c) 2010-2018 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D® SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT

// ParameterList.h

#include "InterfaceServiceWrapper.h"
#include "IEventService.h"
#include "IUnknownHelper.h"
#pragma once
namespace P3D
{
    class P3dCallback : public ICallbackV400
    {
    public:
        P3dCallback() noexcept : m_RefCount(1)  { }
        
        virtual void Invoke(IParameterListV400* pParams) abstract;

        DEFAULT_REFCOUNT_INLINE_IMPL();

        STDMETHODIMP QueryInterface(REFIID riid, PVOID* ppv)
        {
            HRESULT hr = E_NOINTERFACE;

            if (ppv == nullptr)
            {
                return E_POINTER;
            }

            *ppv = NULL;

            if (IsEqualIID(riid, IID_ICallbackV400))
            {
                *ppv = static_cast<ICallbackV400*>(this);
            }
            else if (IsEqualIID(riid, IID_IUnknown))
            {
                *ppv = static_cast<IUnknown*>(this);
            }
            if (*ppv)
            {
                hr = S_OK;
                AddRef();
            }

            return hr;
        };
    };
    
    /**
    *  Basic parameter list with two parameters
    */
    class ParameterList : public IParameterListV400
    {
    public:
        ParameterList(IServiceProvider* pService, UINT64 uParam0, UINT64 uParam1) :
            m_RefCount(1),
            m_pServiceProvider(pService)
        {
            if (pService == nullptr)
            {
                m_pServiceProvider.Attach(new InterfaceServiceWrapper(nullptr));
            }
            m_Param0.Value = uParam0;
            m_Param1.Value = uParam1;
        }

        virtual IServiceProvider* GetServiceProvider()
        {
            return m_pServiceProvider;
        }

        virtual P3DParameter GetParameter(UINT32 index)
        {     
            switch (index)
            {
            case 0:
                return m_Param0;
                break;
            case 1:
                return m_Param1;
                break;
            }
            P3DParameter zeroParam = { 0 };
            return zeroParam;
        }

        virtual UINT32 GetCount()
        {
            return 2;
        }
    protected:
        CComPtr<IServiceProvider> m_pServiceProvider;
        P3DParameter m_Param0;
        P3DParameter m_Param1;
        static const P3DParameter PARAM_ZERO;
        DEFAULT_REFCOUNT_INLINE_IMPL();

        STDMETHODIMP QueryInterface(REFIID riid, PVOID* ppv)
        {
            HRESULT hr = E_NOINTERFACE;

            if (ppv == nullptr)
            {
                return E_POINTER;
            }

            *ppv = NULL;

            if (IsEqualIID(riid, IID_IParameterListV400))
            {
                *ppv = static_cast<IParameterListV400*>(this);
            }
            else if (IsEqualIID(riid, IID_IUnknown))
            {
                *ppv = static_cast<IUnknown*>(this);
            }
            if (*ppv)
            {
                hr = S_OK;
                AddRef();
            }

            return hr;
        };

    };
}