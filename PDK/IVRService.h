// Copyright (c) 2010-2019 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D� SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT

// IVRService.h

#pragma once
#include "IVRPluginSystem.h"
#include "Legacy/IVRService.Legacy.h"

namespace P3D
{
    interface ICallbackV400;
    interface IStereoCameraV500;

    struct VRServiceMessageIDs
    {
        static const UINT64 VR_ENABLED = 0;
        static const UINT64 VR_DISABLED = 1;
        static const UINT64 VR_SETTINGS_SAVED = 2;
        static const UINT64 VR_UPDATE = 3;
    };

    //Enumeration of currently supported HMD Devices
    enum HMD_INTERFACES
    {
        STEAM,
        OCULUS,
        VARJO,
        HMD_EMULATOR,
        HMD_INVALID_INTERFACE
    };

    /** \defgroup vrservice   Virtual Reality Service
     *
     * This Service allows the caller to interact with the Virtual Reality Interface, tracked controllers, access Virtual Reality Settings
     * and register for specific VR callbacks. It also gives the ability to Enable/Disable VR.
     *
     * \{
     */

    DECLARE_INTERFACE_(IVRServiceV452, IVRServiceV450)
    {
        virtual void RegisterCallback(ICallbackV400* pCallback) abstract;
        virtual void UnregisterCallback(ICallbackV400* pCallback) abstract;
        virtual void UnregisterStereoCamera(IStereoCameraV500* pCamera) abstract;
        virtual void RegisterStereoCamera(IStereoCameraV500* pCamera) abstract;

        /**
        * Returns the Virtual Reality Interface
        */
        virtual IVRInterfaceV450* GetVRInterface() = 0;

        /**
        * Returns the Virtual Reality Settings Interface
        */
        virtual IVRSettingsV450* GetVRSettings() = 0;

        /**
        * Enables Virtual Reality with the indicated HMD Device
        * @note, If Virtual Reality is already enabled this function does nothing
        * @param[in] eInterface          Enum for the HMD Device
        */
        virtual void EnableVR(HMD_INTERFACES eInterface) = 0;

        /**
        * Disables Virtual Reality
        */
        virtual void DisableVR() = 0;
    };

    //---------------------------------------------------------------------------
    // Predefined UUIDs

    interface __declspec(uuid("{81a87b88-90b8-435c-a84d-13418bbe8523}")) IVRServiceV452;
    extern __declspec(selectany) REFIID IID_IVRServiceV452 = __uuidof(IVRServiceV452);
    extern __declspec(selectany) REFGUID SID_VRService = __uuidof(IVRServiceV450);

    //--------------------------------------------------------------------------------------------------
    // These typedefs and definitions exist to ease development using these interfaces.  
    // Update to keep in sync with the latest version.
    extern __declspec(selectany) REFIID  IID_IVRService = IID_IVRServiceV452;

    typedef IVRServiceV452 IVRService;

    /** \} */
};