// Copyright (c) 2010-2018 Lockheed Martin Corporation. All rights reserved.
// Use of this file is bound by the PREPAR3D® SOFTWARE DEVELOPER KIT END USER LICENSE AGREEMENT
//----------------------------------------------------------------------------
// INetworkServices.h
// Interface to Prepar3D Network Systems
//----------------------------------------------------------------------------

#pragma once

#include <atlcomcli.h>

namespace P3D
{
    /** \defgroup networkservices   Network Services
     *
     * This PDK service allows callers to manipulate various network systems during runtime.
     * \{
     */

     /**
      * This is the interface to the core Prepar3D multiplayer system. An example on how to query
      * for a PDK service can be found in the DLLStart() function of the Camera PDK Sample.
      */
    DECLARE_INTERFACE_(IMultiplayerServiceV430, IUnknown)
    {
    public:

        /**
        * Returns true if the client is currently connected to a multiplayer session.
        * @remarks The client may still be in the lobby at this point.
        */
        STDMETHOD_(bool, InSession)() const abstract;

        /**
        * Returns true if the client is currently connected to a multiplayer session and is the host.
        * @remarks The client may still be in the lobby at this point.
        */
        STDMETHOD_(bool, IsHosting)() const abstract;

        /**
        * Gets the number of players in the session.
        * @return               Returns the number of players in the current session.
        */
        STDMETHOD_(UINT, GetPlayerCount)() const abstract;

        /**
        * Gets the object ID of the player at the given index.
        * @param uIndex         The index of the player.
        * @return               Returns the player's object ID.
        * @remarks              A return value of 0 is an invalid object ID.
        *                       Indexes are 0 based and the max index will be GetPlayerCount() - 1.
        *                       Multiplayer role indexes may changed as users join and leave the session.
        */
        STDMETHOD_(UINT, GetPlayerObjectID)(__in UINT uIndex) const abstract;

        /**
        * Gets the name of the player at the given index.
        * @param uIndex         The index of the player.
        * @param pszName        The buffer to copy the player name into.
        * @param uLength        The length of the buffer to copy the player name into.
        * @return               Returns S_OK if the name was successfully retrieved and copied, E_FAIL otherwise.
        * @remarks              Indexes are 0 based and the max index will be GetPlayerCount() - 1.
        *                       Multiplayer role indexes may changed as users join and leave the session.
        */
        STDMETHOD(GetPlayerName)(__in UINT uIndex, __out LPWSTR pszName, __in UINT uLength) const abstract;

        /**
        * Gets the player role GUID of the player at the given index.
        * @param uIndex         The index of the player.
        * @param guidRole       The player role GUID.
        * @return               Returns S_OK if the multiplayer role GUID for the given player index was successfully found, E_FAIL otherwise.
        * @remarks              Indexes are 0 based and the max index will be GetPlayerCount() - 1.
        *                       Multiplayer role indexes may changed as users join and leave the session.
        *                       This function only succeeds when in a multiplayer structured scenario.
        *                       
        */
        STDMETHOD(GetPlayerRoleGUID)(__in UINT uIndex, __out GUID& guidRole) const abstract;
    };

    interface __declspec(uuid("{da7f0157-45b8-423a-b1fc-8c01d748b163}")) IMultiplayerServiceV430;
    extern __declspec(selectany) REFIID IID_IMultiplayerServiceV430 = __uuidof(IMultiplayerServiceV430);

};
